import java.util.Scanner;
public class Fahrkartenautomat_Methoden {
	
	public static void main(String[] args) {
		
		for (int i = 1; i > 0; i++) {

			double gesamtpreis= fahrkartenbestellungErfassen ("Ihre Auswahl: ");
			double r�ckgabebetrag = fahrkartenBezahlen(gesamtpreis);

		if(r�ckgabebetrag > 0)
		{ 
			rueckgeldAusgeben (r�ckgabebetrag);
		}
		else
		{
			fahrkartenAusgeben();
		}
		System.out.println("");
		
	}
    
	}

	public static double fahrkartenbestellungErfassen (String eingabe) {
		 Scanner tastatur = new Scanner(System.in);
	     double gesamtpreis = 0;
	     double anzahltickets = 0;
	     int Angabe = 1;
		 int [] Auswahlnummer = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	     String [] Bezeichnung = {"Beenden", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
	     double [] PreisInEuro = {0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
	     
	     System.out.println("Auswahlnummer" + " | " + "Bezeichnung" + " | " + "Preis in Euro");
	     do {
	     
	    	 for(int i = 0; i < Auswahlnummer.length; i++) {
	    	 System.out.println(Auswahlnummer[i] + " | " + Bezeichnung[i] + " | " + PreisInEuro[i]);
	    	}
	     System.out.println("W�hlen Sie ihre Wunschkarte aus:");
	     Angabe = tastatur.nextInt();
	    
	     while(Angabe >=11 && Angabe != 0 || Angabe <0) {
	    	 System.out.println(" >>falsche Eingabe<<");
	    	 System.out.println("Bitte wiederholen sie ihre Auswahl:");
	     }
	   
	     if (Angabe != 0) {
	 	    System.out.print("Anzahl der Tickets: ");
	 	     anzahltickets = tastatur.nextDouble();
	 	     while (anzahltickets<= 0 || anzahltickets >= 11) {

	 	 			System.out.println ("Es gab einen Fehler bei der eingegebenen Ticketanzahl."

	 				+ "\n >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus <<");
	 	 			System.out.println("Anzahl der Tickets: ");
	 	 			anzahltickets=tastatur.nextDouble ();
	 	     }
		     double preis= PreisInEuro[Angabe] * anzahltickets;
		     gesamtpreis = gesamtpreis + preis;
		     Math.round(gesamtpreis*100.0);
		     System.out.println("Zwischensumme: " + gesamtpreis + " Euro");
		    }
		    
		     for (int i = 0; i < 3; i++)
			       {
		    	 System.out.println ("");
			          try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			       }
	     }
		    while (Angabe != 0); 
    return gesamtpreis;
    
}
	     


	public static double fahrkartenBezahlen(double preis) {
		
		double zuZahlenderBetrag = preis;
		System.out.printf("\n%s %.2f %s\n", "Der zu zahlende Betrag ist ", zuZahlenderBetrag, " Euro");
		Scanner tastatur = new Scanner(System.in);
	      double eingezahlterGesamtbetrag = 0.0;
	      double r�ckgabebetrag = 0.0;
	      
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("\n%s %.2f %s\n" ,"Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   double eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       }
	       
	       return r�ckgabebetrag;
	}
	
	public static void fahrkartenAusgeben () {
		 System.out.println("\nFahrscheine werden ausgegeben");
		 
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       
	       System.out.println("\n");
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir w�nschen Ihnen eine gute Fahrt.");
	       
	       for (int i = 0; i < 2; i++)
	       {
	          try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
			}
	
	public static void rueckgeldAusgeben (double rueckgeld) {
			 System.out.println("\nFahrschein wird ausgegeben");
			 
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       
		       System.out.println("\n");
	    	   System.out.printf("\n%s %.2f %s\n" ,"Der R�ckgabebetrag in H�he von ", rueckgeld, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(Math.round(rueckgeld*100.0)/100.0 >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgeld -= 2.0;
	           }
	           
	           while(Math.round(rueckgeld*100.0)/100.0 >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgeld -= 1.0;
	           }
	           
	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgeld -= 0.5;
	           }
	           
	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgeld -= 0.2;
	           }
	           
	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgeld -= 0.1;
	           }
	           
	           while(Math.round(rueckgeld*100.0)/100.0 >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgeld -= 0.05;
	           }
	           
	           
	           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                      "vor Fahrtantritt entwerten zu lassen!\n"+
                      "Wir w�nschen Ihnen eine gute Fahrt.");
	           for (int i = 0; i < 2; i++)
		       {
		          try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
	           
	           for (int i = 0; i < 8; i++)
		       {
		          System.out.println("");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
	           
	}
	
}