import java.util.Scanner;

public class Mittelwert {
	static double berechneMittelwert(double x, double y)
	{
		return (x + y) / 2;
	}
	public static void main(String[] args) {
			
		 Scanner tastatur = new Scanner(System.in);
 
	// (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double a;
      double b;
      double m;
      
      System.out.print("Zahl 1: ");
      a = tastatur.nextDouble();
      
      System.out.print("Zahl 2: ");
      b = tastatur.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = berechneMittelwert (a, b);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", a, b, m);

	}
}
