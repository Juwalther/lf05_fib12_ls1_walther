import java.util.Scanner;
public class Testing {
	
	public static void main(String[] args) {
		double preis= fahrkartenbestellungErfassen ("Der Ticketpreis ist: ");
		double anzahl= fahrkartenbestellungErfassen ("Die Ticketanzahl ist: ");
		if (anzahl > 10)
		{
			anzahl = 1;
			System.out.println ("Es gab einen Fehler bei der eingegebenen Ticketanzahl."
					+ "\n Die Anzahl wurde auf 1 gesetzt.");
		}
		else if (0 <= anzahl)
		{
			anzahl = 1;
			System.out.println ("Es gab einen Fehler bei der eingegebenen Ticketanzahl."
					+ "\nDie Anzahl wurde auf 1 gesetzt.");	
		}
		double r�ckgabebetrag = fahrkartenBezahlen(preis, anzahl);
		if(r�ckgabebetrag > 0.0)
		{ 
			rueckgeldAusgeben (r�ckgabebetrag);
		}
		else
		{
			fahrkartenAusgeben();
		}
    
	}
	public static double fahrkartenbestellungErfassen (String eingabe) {
		 Scanner tastatur1 = new Scanner(System.in);
		 System.out.print(eingabe);
	     double Zahl1 = tastatur1.nextDouble();
	     return Zahl1;
	}
	
	public static double fahrkartenBezahlen(double x, double y) {
		double zuZahlenderBetrag = x * y;
		System.out.printf("\n%s %.2f %s\n", "Der zu zahlende Betrag ist ", zuZahlenderBetrag, " Euro");
		Scanner tastatur = new Scanner(System.in);
	      double eingezahlterGesamtbetrag = 0.0;
	      double r�ckgabebetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("\n%s %.2f %s\n" ,"Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   double eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       }
	       return r�ckgabebetrag;
	}
	
	
	public static void fahrkartenAusgeben () {
	       System.out.println("\n");
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
	public static void rueckgeldAusgeben (double z) {
		       System.out.println("\n");
	    	   System.out.printf("\n%s %.2f %s\n" ,"Der R�ckgabebetrag in H�he von ", z, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(z >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          z = z - 2.0;
	           }
	           while(z >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          z = z - 1.0;
	           }
	           while(z >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          z = z - 0.5;
	           }
	           while(z >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          z = z - 0.2;
	           }
	           while(z >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          z = z - 0.1;
	           }
	           while(z >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          z = z - 0.05;
	           }
	           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                      "vor Fahrtantritt entwerten zu lassen!\n"+
                      "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
}
