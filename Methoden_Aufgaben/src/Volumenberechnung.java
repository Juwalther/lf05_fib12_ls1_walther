import java.util.Scanner;
public class Volumenberechnung {
	public static double volumenWuerfel(double a) {
		return a * a * a;	
	}
	public static double volumenQuader(double a, double b, double c) {
		return a * b * c;	
	}
	public static double volumenPyramide(double a, double h) {
		return a * a * h/3;	
	}
	public static double volumenKugel(double r) {
		return (4 / 3) * Math.PI * Math.pow(r, 3);	
	}
	
	public static void main (String[] args) {
		 Scanner tastatur = new Scanner(System.in);
		 System.out.print("Wert a: ");
		double a = tastatur.nextDouble();
		System.out.print("Wert b: ");
		double b = tastatur.nextDouble();
		System.out.print("Wert c: ");
		double c = tastatur.nextDouble();
		System.out.print("Wert h: ");
		double h = tastatur.nextDouble();
		System.out.print("Wert r: ");
		double r = tastatur.nextDouble();
		
		System.out.println("Das Volumen des W�rfels ist " + volumenWuerfel(a));
		System.out.println("Das Volumen des Quaders ist " + volumenQuader(a, b, c));
		System.out.println("Das Volumen des Pyramide ist " + volumenPyramide(a, h));
		System.out.println("Das Volumen der Kugel ist " + volumenKugel(r));
		
	
	}
}
