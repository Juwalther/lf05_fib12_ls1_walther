import java.util.*;
public class Simulation {

	
	public static void main(String[] args) {
		
		Ladung lk1 = new Ladung("Ferengi Schneckensaft", 200);  //lk = Ladung Klingonene
		Ladung lk2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung lr1 = new Ladung("Borg-Schrott", 5);  //lr = Ladung Romulaner
		Ladung lr2 = new Ladung("Rote Materie", 2);
		Ladung lr3 = new Ladung("Plasma-Waffe", 50);
		Ladung lv1 = new Ladung("Forschungssonde", 35); //lv = Ladung Vulkanier
		Ladung lv2 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff r1 = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff r2 = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff r3 = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);
		
		r1.beladen(lk1);
		r1.beladen(lk2);
		r2.beladen(lr1);
		r2.beladen(lr2);
		r2.beladen(lr3);
		r3.beladen(lv1);
		r3.beladen(lv2);
		
		r3.zustandRaumschiff();
		r2.ausgabeLadungsverzeichnis();
		
		r1.photonentorpedosAbschiessen(r3);
		r1.phaserkanonenAbschiessen(r3);
		
		System.out.println("Logbuch " + r1.getSchiffname()+ ":" + r1.logbuchZurueckgeben());
		System.out.println("Logbuch " + r2.getSchiffname()+ ":" + r2.logbuchZurueckgeben());
		System.out.println("Logbuch " + r3.getSchiffname()+ ":" + r3.logbuchZurueckgeben());
	}

}
