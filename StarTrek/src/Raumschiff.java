import java.util.*;
public class Raumschiff {
	
	private String schiffname;
	private double energieversorgung;
	private double schutzschilde;
	private double lebenserhaltungssysteme;
	private double huelle;
	private int photonentorpedos;
	private int reperaturdroiden;
	private ArrayList<String> broadcastKommunikator = new ArrayList();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList();
	
	//Konstruktoren
	public Raumschiff() {
		this.schiffname = "unbekannt";
		this.energieversorgung = 0;
		this.schutzschilde = 0;
		this.lebenserhaltungssysteme = 0;
		this.energieversorgung = 0;
		this.huelle = 0;
		this.photonentorpedos = 0;
		this.reperaturdroiden = 0;
	}
	public Raumschiff (String schiffname, double energieversorgung, double schutzschilde, double lebenserhaltungssysteme, double huelle, int photonentorpedos, int reperaturdroiden) {
		this.schiffname = schiffname;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.energieversorgung = energieversorgung;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reperaturdroiden = reperaturdroiden;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	//Getter und Setter

	public String getSchiffname() {
		return schiffname;
	}
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}
	public double getEnergieversorgung() {
		return energieversorgung;
	}
	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	public double getSchutzschilde() {
		return schutzschilde;
	}
	public void setSchutzschilde(double schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	public double getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	public void setLebenserhaltungssyteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	public double getHuelle() {
		return huelle;
	}
	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	public int getReperaturdroiden() {
		return reperaturdroiden;
	}
	public void setReperaturdroiden(int reperaturdroiden) {
		this.reperaturdroiden = reperaturdroiden;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	//andere Methoden
	public void photonentorpedosAbschiessen (Raumschiff zielRaumschiff) {
		if (photonentorpedos<=0) {
			nachrichtSenden("-=*Click*=-");
		}
		else {
			photonentorpedos = photonentorpedos - 1;
			nachrichtSenden("Photonentorpedo abgeschossen");
			zielRaumschiff.trefferVermerken(zielRaumschiff);
		}
	}
	
	public void phaserkanonenAbschiessen (Raumschiff zielRaumschiff) {
		if (energieversorgung<=50) {
			nachrichtSenden("-=*Click*=-");
		}
		else {
			energieversorgung = energieversorgung - 50;
			nachrichtSenden("Phaserkanone abgeschossen");
			zielRaumschiff.trefferVermerken(zielRaumschiff);
		}
	}
	
	private void trefferVermerken(Raumschiff ziel) {
		System.out.println(schiffname + " wurde getroffen");
		int trefferAnzahl = 0;
		trefferAnzahl = trefferAnzahl + 1;
		
		genommenerSchaden(trefferAnzahl, 50);

	}

	
	private void genommenerSchaden (int treffer, double ausmassSchaden) {
		for (int hits=treffer; hits>0; hits--) {
		if (schutzschilde>50) {
			schutzschilde = schutzschilde -ausmassSchaden;
		}
		else {
			schutzschilde = 0;
			huelle = huelle - ausmassSchaden;
			energieversorgung = energieversorgung - ausmassSchaden;
			if (huelle >= 0) {
				lebenserhaltungssysteme = 0;
				nachrichtSenden("Die Lebenserhaltungssysteme der Raumschiffes " + schiffname + " wurden vollständig zerstört");
			}
		}
		}
	}
	
	public void nachrichtSenden (String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	
	public void beladen (Ladung aufzuladeneLadung) {
		ladungsverzeichnis.add(aufzuladeneLadung);
	}
	
	public ArrayList<String> logbuchZurueckgeben() {
        return broadcastKommunikator;
	}
	
	public void photonentorpedoesLaden (int anzahlTorpedos) {
		
	}
	
	public void reperaturAuftrag(boolean schutzschilde, boolean energieversorgung, boolean schiffhuelle, int anzahlAndroiden) {
		
	}
	
	public void zustandRaumschiff () {
		System.out.println("Zustand des Raumshiffes " + schiffname + ":");
		System.out.println("Die Energieversorung liegt bei " + energieversorgung + "%");
		System.out.println("Die Schutzschilde liegen bei " + schutzschilde + "%");
		System.out.println("Die Lebenserhaltungssysteme liegen bei " + lebenserhaltungssysteme + "%");
		System.out.println("Die Huelle liegt bei " + huelle + "%");
		System.out.println("Die Anzahl der Photonentorpedos liegt bei " + photonentorpedos);
		System.out.println("Die Anzahl der Reperaturdroiden liegt bei " + reperaturdroiden);
	}
	
	public void ausgabeLadungsverzeichnis() {
		System.out.println();
		System.out.println("Das Raumschiff " + schiffname + " besitzt folgende Ladung:");
		for (int i = 0; i<ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}
		System.out.println();
	}
	
	public void aufräumenLadungsverzeichnis() {
		
	}
	
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

}
