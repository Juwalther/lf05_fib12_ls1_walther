
public class Ladung {
	
	private String ladungsart;
	private int anzahl;
	
	//Konstruktoren
	public Ladung() {
		this.ladungsart = "unbekannt";
		this.anzahl = 0;
	}
	public Ladung(String ladungsart, int anzahl) {
		this.ladungsart = ladungsart;
		this.anzahl = anzahl;
	}
	
	// Getter und Setter
	public String getLadungsart() {
		return ladungsart;
	}
	public void setLadungsart(String ladungsart) {
		this.ladungsart = ladungsart;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	//andere Methoden
	public String toString() {
		return this.ladungsart + ", St�ckzahl " + this.anzahl;
	}
}
