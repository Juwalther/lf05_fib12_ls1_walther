import java.util.Scanner;

public class Summe {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);		
		int eingabe;
		
//		Geben Sie bitte einen begrenzenden Wert ein: 6
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		eingabe = tastatur.nextInt();
		int SummeA = 0;
		int SummeB = 0;
		int SummeC = 0;
		
//		a) 1 + 2 + 3 + 4 +�+ n for-Schleife / while-Schleife 
		for (int a = 1; a <= eingabe; a++) {
		SummeA = SummeA + a;	
		}
		System.out.println("Die Summe f�r A betr�gt: " + SummeA);
//		Die Summe f�r A betr�gt: 21 

		
//		b) 2 + 4 + 6 +�+ 2n for-Schleife / while-Schleife 
		int b = 1;
		while (b <= eingabe) {
		SummeB = SummeB + (2*b);
		b++;
		}
		System.out.println("Die Summe f�r B betr�gt: " + SummeB);	
//		Die Summe f�r B betr�gt: 42  
		
	
//		c) 1 + 3 + 5 +�+ (2n+1) for-Schleife / while-Schleife 
		for (int c = 0; c <= eingabe; c++) {
		SummeC = SummeC + (2*c+1);
		}
		System.out.println("Die Summe f�r C betr�gt: " + SummeC);	
//		Die Summe f�r C betr�gt: 49
	}
}
